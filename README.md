﻿# **High Data Ingestion**

## Test script

Download _readefficiently_ project and run *mvn clean package -DskipTests* to compile the executable jar.

To run the _readefficiently.sh_ script, the following jars must be in the same directory: _lxjdbcdriver.jar_ and _readefficiently.jar_

