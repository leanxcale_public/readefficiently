package com.leanxcale.readefficiently;

public class Main {

	private static String[] tables = {
		"PAYMENTS1", // no partitioning, meaningless primary key
		"PAYMENTS2", // plus src_acct as first primary key field
		"PAYMENTS3", // plus partitioning
		"PAYMENTS4", // plus index on dst_acct
		"PAYMENTS5"  // plus online aggregate
	};

	// all tables have the same structure and contain synthetic data:
	// - pmt_id: unique integer to serve as (part of the) primary key
	// - pmt_ts: timestamp, randomly generated between 2014-01-01 and 2023-12-31
	// - src_acct: randomly generated 10-character numeric string, i.e. between '0000000000' and '9999999999'
	// - dst_acct: another randomly generated 10-character numeric string, i.e. between '0000000000' and '9999999999'
	// - amount: randomly generated double between 1 and 1000

	private static String[] ddls = {
		// no partitioning, meaningless primary key
		"CREATE TABLE PAYMENTS1 (pmt_id bigint, pmt_ts timestamp, src_acct varchar, dst_acct varchar, amount double, PRIMARY KEY(pmt_id))",

		// with src_acct as first primary key field
		"CREATE TABLE PAYMENTS2 (pmt_id bigint, pmt_ts timestamp, src_acct varchar, dst_acct varchar, amount double, PRIMARY KEY(src_acct, pmt_id))",

		// with partitioning
		"CREATE TABLE PAYMENTS3 (pmt_id bigint, pmt_ts timestamp, src_acct varchar, dst_acct varchar, amount double, PRIMARY KEY(src_acct, pmt_id))"
			+ " PARTITION BY key(src_acct) at ('25'), ('50'), ('75')",

		// with index on dst_acct
		"CREATE TABLE PAYMENTS4 (pmt_id bigint, pmt_ts timestamp, src_acct varchar, dst_acct varchar, amount double, PRIMARY KEY(src_acct, pmt_id))"
			+ " PARTITION BY key(src_acct) at ('25'), ('50'), ('75')",
		"CREATE INDEX PAYMENTS4_IDX ON payments4 (dst_acct)",

		// with online aggregate
		"CREATE TABLE PAYMENTS5 (pmt_id bigint, pmt_ts timestamp, src_acct varchar, dst_acct varchar, amount double, PRIMARY KEY(src_acct, pmt_id))"
			+ " PARTITION BY key(src_acct) at ('25'), ('50'), ('75')",
		"CREATE INDEX PAYMENTS5_IDX ON payments5 (dst_acct)",
		"CREATE ONLINE AGGREGATE PAYMENTS5_OA AS SUM(amount) AS amount FROM payments5" 
			+ " GROUP BY EXTRACT(YEAR FROM pmt_ts) AS y, EXTRACT(MONTH FROM pmt_ts) AS m, dst_acct",
	};

	private static String Q1_Naive = "SELECT src_acct, dst_acct, amount FROM payments1 WHERE src_acct = ? AND extract(year from pmt_ts) = ? AND extract(month from pmt_ts) = ?";
	private static String Q1_WithPK = "SELECT src_acct, dst_acct, amount FROM payments2 WHERE src_acct = ? AND extract(year from pmt_ts) = ? AND extract(month from pmt_ts) = ?";
	private static String Q1_WithPK_Partitions = "SELECT src_acct, dst_acct, amount FROM payments3 WHERE src_acct = ? AND extract(year from pmt_ts) = ? AND extract(month from pmt_ts) = ?";
	private static String Q2_NoIndex = "SELECT src_acct, dst_acct, amount FROM payments3 WHERE dst_acct = ? AND extract(year from pmt_ts) = ? AND extract(month from pmt_ts) = ?";
	private static String Q2_WithIndex = "SELECT src_acct, dst_acct, amount FROM payments4 WHERE dst_acct = ? AND extract(year from pmt_ts) = ? AND extract(month from pmt_ts) = ?";
	private static String Q3_NoOA = "SELECT dst_acct, SUM(amount) AS amount FROM payments4 WHERE extract(year from pmt_ts) = ? AND extract(month from pmt_ts) = ? GROUP BY dst_acct";
	private static String Q3_WithOA = "SELECT dst_acct, SUM(amount) AS amount FROM payments5 WHERE extract(year from pmt_ts) = ? AND extract(month from pmt_ts) = ? GROUP BY dst_acct";
	private static String Q4_NoOA = "SELECT extract(month from pmt_ts), dst_acct, SUM(amount) AS amount FROM payments4 WHERE extract(year from pmt_ts) = ? GROUP BY extract(month from pmt_ts), dst_acct";
	private static String Q4_WithOA = "SELECT extract(month from pmt_ts), dst_acct, SUM(amount) AS amount FROM payments5 WHERE extract(year from pmt_ts) = ? GROUP BY extract(month from pmt_ts), dst_acct";
	private static String Q5_NoStats = "SELECT payments.src_acct, payments.dst_acct, payments.amount, payments.pmt_ts"
				+ " FROM"
				+ " (SELECT dst_acct, SUM(amount) AS amount FROM payments5 WHERE extract(year from pmt_ts) = ? AND extract(month from pmt_ts) = ? GROUP BY dst_acct ORDER BY 2 DESC LIMIT 10) topk"
				+ " JOIN payments4 AS payments ON topk.dst_acct = payments.dst_acct WHERE extract(year from pmt_ts) = ? AND extract(month from pmt_ts) = ?";
	private static String Q5_WithStats = "SELECT payments.src_acct, payments.dst_acct, payments.amount, payments.pmt_ts"
				+ " FROM"
				+ " (SELECT dst_acct, SUM(amount) AS amount FROM payments5 WHERE extract(year from pmt_ts) = ? AND extract(month from pmt_ts) = ? GROUP BY dst_acct ORDER BY 2 DESC LIMIT 10) topk"
				+ " JOIN payments5 AS payments ON topk.dst_acct = payments.dst_acct WHERE extract(year from pmt_ts) = ? AND extract(month from pmt_ts) = ?";
	private static String Q_RunStats = "ANALYZE STATISTICS FOR TABLE PAYMENTS5";

	public static void main(String[] args) {
		Utils.init();
		try {
			Utils.loadData(tables, ddls);
			Utils.runquery(1, 4, Q1_Naive, "Q1_Naive");
			Utils.runquery(1, 64, Q1_WithPK, "Q1_WithPK");
			Utils.runquery(1, 64, Q1_WithPK_Partitions, "Q1_WithPK_Partitions");
			Utils.runquery(4, 64, Q1_WithPK_Partitions, "Q1_WithPK_Partitions");
			Utils.runquery(4, 4, Q2_NoIndex, "Q2_NoIndex");
			Utils.runquery(4, 64, Q2_WithIndex, "Q2_WithIndex");
			Utils.runquery(4, 4, Q3_NoOA, "Q3_NoOA");
			Utils.runquery(4, 64, Q3_WithOA, "Q3_WithOA");
			Utils.runquery(4, 4, Q4_NoOA, "Q4_NoOA");
			Utils.runquery(4, 64, Q4_WithOA, "Q4_WithOA");
			Utils.runquery(4, 4, Q5_NoStats, "Q5_NoStats");
			Utils.connect(true).createStatement().execute(Q_RunStats);
			Utils.runquery(4, 64, Q5_WithStats, "Q5_WithStats");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
