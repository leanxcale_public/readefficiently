package com.leanxcale.readefficiently;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Random;

public class Utils {
	private static int acctlen = 10;
	private static int nrows = 10000000;
	private static int naccts = 10000;
	private static int maxamount = 1000;
	private static int batchsz = 100;
	private static boolean create = false;

	private static Random rnd = null;
	private static String[] accts = null;
	private static boolean inited = false;

	public static void init() {
		String props = System.getenv("READEFF_PROPS");
		if (props != null) {
			for (String prop : props.split(";")) {
				String[] pv = prop.split("=");
				if (pv[0].equals("NROWS")) {
					nrows = Integer.valueOf(pv[1]);
				}
				if (pv[0].equals("NACCTS")) {
					naccts = Integer.valueOf(pv[1]);
				}
				if (pv[0].equals("BSZ")) {
					batchsz = Integer.valueOf(pv[1]);
				}
				if (pv[0].equals("CREATE")) {
					create = true;
				}
			}
		}
		rnd = new Random(1029384756);
		accts = new String[naccts];
		for (int i = 0 ; i < naccts ; ++i) {
			accts[i] = genAcct();
		}
		inited = true;
	}

	private static String genAcct() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0 ; i < acctlen ; ++i) {
			int r = rnd.nextInt(10);
			sb.append(String.valueOf(r));
		}
		return sb.toString();
	}

	private static String rndAcct() {
		return accts[rnd.nextInt(naccts)];
	}

	private static int rndMonth() {
		return rnd.nextInt(10)+3;
	}

	private static int rndYear() {
		return rnd.nextInt(10)+2014;
	}

	public static Connection connect(boolean autocommit) throws Exception {
		if (!inited) {
			throw new RuntimeException("not initialized; call Utils.init() first");
		}
		String lxUrl = "jdbc:leanxcale://localhost:14420/db";
		String lxUser = "lxadmin";
		String  lxPwd = "foobar";
		Connection conn = DriverManager.getConnection(lxUrl, lxUser, lxPwd);
		conn.setAutoCommit(autocommit);
		return conn;
	}

	private static Connection connect() throws Exception {
		return connect(false);
	}

	private static boolean tblExists(Connection conn, String tbl) {
		try (ResultSet rs = conn.getMetaData().getTables(null, null, tbl.toUpperCase(), null)) {
			return rs.next();
		} catch (Exception e) {
			return false;
		}
	}

	private static boolean tblsExistAll(Connection conn, String[] tbls) {
		for (String tbl : tbls) {
			if (!tblExists(conn, tbl)) {
				return false;
			}
		}
		return true;
	}

	public static void loadData(String[] tables, String[] ddls) throws Exception {
		try (Connection conn = connect(); Statement s = conn.createStatement()) {
			if (!create && tblsExistAll(conn, tables)) {
				return;
			}
			for (String tbl : tables) {
				s.execute("DROP TABLE IF EXISTS " + tbl);
			}
			for (String ddl : ddls) {
				s.execute(ddl);
			}
		}
		populate(20, tables);
	}

	private static Object[] genRow(int id) {
		Object[] r = new Object[5];
		r[0] = id;
		// random date between 2014-01-01 and 2023-12-31, time 12:00:00
		r[1] = 1388570400000L + rnd.nextInt(3652)*86400000L;
		r[2] = rndAcct();
		r[3] = rndAcct();
		r[4] = rnd.nextDouble()*maxamount;
		return r;
	}

	private static void xpopulate(int nthrs, int j, String[] tbls) throws Exception {
		try (Connection c = connect()) {
			PreparedStatement[] ps = new PreparedStatement[tbls.length];
			for (int t = 0 ; t < tbls.length ; ++t) {
				ps[t] = c.prepareStatement("INSERT INTO " + tbls[t] + " VALUES (?, ?, ?, ?, ?)");
			}
			long t0 = System.currentTimeMillis();
			for (int i = 0 ; i < nrows/nthrs ; ++i) {
				Object[] row = genRow(i*nthrs+j);
				for (int t = 0 ; t < tbls.length ; ++t) {
					for (int f = 0 ; f < row.length ; ++f) {
						ps[t].setObject(f+1, row[f]);
					}
					ps[t].executeUpdate();
				}
				if (i%batchsz == batchsz-1) {
					long t1 = System.currentTimeMillis();
					System.out.println((i+1) + " rows inserted in " + (t1-t0)/1000 + "s");
					c.commit();
				}
			}
			c.commit();
		}
	}

	private static void populate(int nthrs, String[] tbls) throws Exception {
		Thread[] thrs = new Thread[nthrs];
		Exception[] xs = new Exception[nthrs];
		long t0 = System.currentTimeMillis();
		for (int i = 0 ; i < nthrs ; ++i) {
			final int j = i;
			thrs[i] = new Thread(()->{
				try {
					xpopulate(nthrs, j, tbls);
				} catch (Exception e) {
					xs[j] = e;
				}
			});
			thrs[i].start();
		}
		for (int i = 0 ; i < nthrs ; ++i) {
			thrs[i].join();
		}
		throwFirst(xs);
		long t1 = System.currentTimeMillis();
		System.out.println("POPULATE: " + (double)(t1-t0) + "ms");
	}

	private static void xrunquery(int nruns, String sql, int j, int[][] times, int[] nrows) throws Exception {
		try (Connection c = connect(); PreparedStatement ps = c.prepareStatement(sql)) {
			int nparams = ps.getParameterMetaData().getParameterCount();
			int r = 0;
			for (int i = 0 ; i < nruns ; ++i) {
				long t0 = System.currentTimeMillis();
				if (nparams==1) {
					ps.setLong(1, rndYear());
				} else if (nparams==2) {
					ps.setLong(1, rndYear());
					ps.setLong(2, rndMonth());
				} else if (nparams==3) {
					ps.setString(1, rndAcct());
					ps.setLong(2, rndYear());
					ps.setLong(3, rndMonth());
				} else if (nparams==4) {
					long y = rndYear();
					long m = rndMonth();
					ps.setLong(1, y);
					ps.setLong(2, m);
					ps.setLong(3, y);
					ps.setLong(4, m);
				}
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					r++;
				}
				rs.close();
				times[j][i] = (int)(System.currentTimeMillis() - t0);
			}
			Arrays.sort(times[j]);
			nrows[j] = r;
		}
	}

	private static String fmttimes(int[][] times) {
		int nthrs = times.length;
		int nruns = times[0].length;
		StringBuilder sb = new StringBuilder();
		for (int p = 25; p <= 100; p += 25) {
			int u = Math.max(1, nruns*p/100);
			double q = (double)Arrays.stream(times).mapToInt(t -> t[u-1]).sum()/(nthrs*nthrs);
			double avg = (double)Arrays.stream(times).mapToInt(t -> Arrays.stream(Arrays.copyOf(t, u)).sum()).sum()/(nthrs*nthrs*u);
			sb.append("Q").append(p).append(" = ").append(q).append("ms; ");
			sb.append("AVG").append(p).append(" = ").append(avg).append("ms; ");
		}
		return sb.toString();
	}

	public static void runquery(int nThreads, int nRuns, String sql, String tag) throws Exception {
		System.out.println("\nRUNNING " + tag + " " + (nThreads*nRuns) + " times in " + nThreads + " thread(s)..........");
		try (Connection c = connect(); ResultSet rs = c.createStatement().executeQuery("explain plan for " + sql)) {
			rs.next();
			System.out.println(sql);
			System.out.println(rs.getString(1));
		}
		Thread[] thrs = new Thread[nThreads];
		Exception[] xs = new Exception[nThreads];
		int[][] times = new int[nThreads][nRuns];
		int[] nrows = new int[nThreads];
		long t0 = System.currentTimeMillis();
		for (int i = 0 ; i < nThreads ; ++i) {
			final int j = i;
			thrs[i] = new Thread(()->{
				try {
					xrunquery(nRuns, sql, j, times, nrows);
				} catch (Exception e) {
					xs[j] = e;
				}
			});
			thrs[i].start();
		}
		for (int i = 0 ; i < nThreads ; ++i) {
			thrs[i].join();
		}
		throwFirst(xs);
		long t1 = System.currentTimeMillis();
		int nrs = Arrays.stream(nrows).sum();
		int n = nThreads*nRuns;
		System.out.println("QUERY " + tag + ": avg rows = " + (double)nrs/n + "; "
			// + fmttimes(times)
			+ "avg total time = " + (double)(t1-t0)/n + "ms\n");
	}

	private static void throwFirst(Exception[] xs) throws Exception {
		for (Exception x : xs) {
			if (x != null) {
				throw x;
			}
		}
	}

}
